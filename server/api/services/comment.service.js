import commentRepository from '../../data/repositories/comment.repository';
import commentReactionRepository from '../../data/repositories/comment-reaction.repository';

export const getCommentById = id => commentRepository.getCommentById(id);

export const create = (userId, comment) => commentRepository.create({
    ...comment,
    userId
});

export const update = async (userId, commentId, comment) => {
    const existComment = await commentRepository.getIfUserIsOwner(userId, commentId);

    return commentRepository.updateById(commentId, {
        ...existComment,
        ...comment
    });
};

export const remove = async (userId, commentId) => {
    const existComment = await commentRepository.getIfUserIsOwner(userId, commentId);

    return commentRepository.deleteById(existComment.id);
};

export const setReaction = async (userId, { commentId, isLike = true, isDislike = false }) => {
    const reactionData = { isLike: isDislike ? false : isLike, isDislike };
    const updateOrDelete = react => (
        react.isLike === reactionData.isLike || react.isDislike === reactionData.isDislike
            ? commentReactionRepository.deleteById(react.id)
            : commentReactionRepository.updateById(react.id, reactionData)
    );

    const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

    const result = reaction
        ? await updateOrDelete(reaction)
        : await commentReactionRepository.create({ userId, commentId, ...reactionData });

    return Number.isInteger(result) ? {} : commentReactionRepository.getCommentReaction(userId, commentId);
};
