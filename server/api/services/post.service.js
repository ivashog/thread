import postRepository from '../../data/repositories/post.repository';
import postReactionRepository from '../../data/repositories/post-reaction.repository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
    ...post,
    userId
});

export const update = async (userId, postId, post) => {
    const existPost = await postRepository.getIfUserIsOwner(userId, postId);

    return postRepository.updateById(postId, {
        ...existPost,
        ...post
    });
};

export const remove = async (userId, postId) => {
    const existPost = await postRepository.getIfUserIsOwner(userId, postId);

    return postRepository.deleteById(existPost.id);
};

export const setReaction = async (userId, { postId, isLike = true, isDislike = false }) => {
    const reactionData = { isLike: isDislike ? false : isLike, isDislike };
    const updateOrDelete = react => (
        react.isLike === reactionData.isLike || react.isDislike === reactionData.isDislike
            ? postReactionRepository.deleteById(react.id)
            : postReactionRepository.updateById(react.id, reactionData));

    const reaction = await postReactionRepository.getPostReaction(userId, postId);

    if (reaction) await updateOrDelete(reaction);
    else await postReactionRepository.create({ userId, postId, ...reactionData });

    return postRepository.getReactionsCountByPostId(postId);
};
