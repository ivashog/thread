import { Router } from 'express';
import * as commentService from '../services/comment.service';

const router = Router();

router
    .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
        .then(comment => res.send(comment))
        .catch(next))
    .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
        .then(comment => res.send(comment))
        .catch(next))
    .patch('/:id', (req, res, next) => commentService.update(req.user.id, req.params.id, req.body)
        .then(comment => res.send(comment))
        .catch(next))
    .delete('/:id', (req, res, next) => commentService.remove(req.user.id, req.params.id)
        .then(comment => res.status(204).end())
        .catch(next))
    .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
        .then((reaction) => {
            if (reaction.post && (reaction.post.userId !== req.user.id)) {
                // notify a user if someone (not himself) liked his post
                req.io.to(reaction.post.userId).emit('like', 'Your comment was liked!');
            }
            return res.send(reaction);
        })
        .catch(next));

export default router;
