export default class BaseRepository {
    constructor(model) {
        this.model = model;
    }

    getAll() {
        return this.model.findAll();
    }

    getById(id) {
        return this.model.findByPk(id);
    }

    async getIfUserIsOwner(userId, id) {
        const existData = await this.model.findByPk(id);

        if (!existData) throw new Error(`${this.model.name} with id '${id}' is not exist!`);
        if (userId !== existData.userId) {
            throw new Error(`User id '${userId}' is not owner of ${this.model.name} id '${id}'`);
        }

        return existData;
    }

    create(data) {
        return this.model.create(data);
    }

    async updateById(id, data) {
        const result = await this.model.update(data, {
            where: { id },
            returning: true,
            plain: true
        });

        return result[1];
    }

    deleteById(id) {
        return this.model.destroy({
            where: { id }
        });
    }
}
